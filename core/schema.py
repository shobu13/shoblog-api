from datetime import datetime
import typing

import strawberry

from core.models import Tag, User, Content
import strawberry_django
from strawberry import auto


@strawberry_django.type(Tag)
class TagType:
    id: auto
    name: auto

@strawberry_django.filters.filter(Tag)
class TagFilter:
    id: auto
    name: auto

@strawberry_django.type(User)
class UserType:
    id: auto
    username: auto


@strawberry_django.interface(Content)
class ContentInterface:
    name: auto
    slug: auto
    date_creation: auto
    last_modification: auto
    owner: UserType

    tags: typing.List[TagType]


@strawberry.type
class Query:
    tags: typing.List[TagType] = strawberry.field()
    tag: TagType = strawberry.field()
    users: typing.List[UserType]
    user: UserType
