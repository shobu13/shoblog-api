# Generated by Django 3.2 on 2021-04-29 22:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("article", "0002_auto_20210429_1827"),
    ]

    operations = [
        migrations.AddField(
            model_name="article",
            name="thumbnail",
            field=models.ImageField(default=None, upload_to="article_thumbnail"),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="galerieimage",
            name="galerie",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="images",
                to="article.galerie",
            ),
        ),
    ]
