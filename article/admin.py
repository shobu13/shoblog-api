from django.contrib import admin
from nested_inline.admin import NestedStackedInline, NestedTabularInline

from article.models import Article, ArticleImage, Comment
from core.admin import ContentAdmin

class ArticleImageInline(admin.StackedInline):
    model = ArticleImage

    extra=0
    classes = ['collapse']

class ResponseInline(NestedStackedInline):
    model = Comment
    
    extra=0
    fk_name="comment"
    exclude = ("article",)

class CommentInline(NestedStackedInline):
    model = Comment
    
    extra=0
    inlines = [ResponseInline]
    exclude = ("comment",)

@admin.register(Article)
class ArticleAdmin(ContentAdmin):
    inlines = [
        ArticleImageInline,
        CommentInline
    ]

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    pass
