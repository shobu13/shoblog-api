from django.shortcuts import render

from .models import Article

def article(request, slug):
    return render(request, "article.html", context={"article":Article.objects.filter(slug=slug).first()})