import typing
import dataclasses

from django.core.exceptions import ObjectDoesNotExist
import strawberry
import strawberry_django
from strawberry import auto, lazy, ID

from article.models import Article, Comment
from core.schema import ContentInterface, TagFilter

@strawberry.type
class DjangoImageType:
    name: str
    url: str
    width: int
    height: int
    size: int

@strawberry_django.type(Comment)
class CommentType:
    id: auto
    author: auto
    creation_date: auto
    text: auto
    article: typing.Annotated["ArticleType", lazy(".schema")]
    responses: list[typing.Annotated["CommentType", lazy(".schema")]]

@strawberry_django.input(Comment)
class CommentInput:
    author: auto
    text: auto

@strawberry_django.ordering.order(Article)
class ArticleOrder:
    date_creation: auto

@strawberry_django.filters.filter(Article, lookups=True)
class ArticleFilter:
    tags: TagFilter

    tagged: list[int] | None

    def filter_tagged(self, queryset):
        if self.tagged is None:
            return queryset
        
        for i in self.tagged:
            queryset = queryset.filter(tags__in=[i])
        return queryset


@strawberry_django.type(Article, pagination=True, order=ArticleOrder, filters=ArticleFilter)
class ArticleType(ContentInterface):
    id: auto
    thumbnail: DjangoImageType
    text: auto
    headline: auto
    comments: list[CommentType]


@strawberry.type
class Query:
    articles: typing.List[ArticleType] = strawberry_django.field()
    article: ArticleType  = strawberry_django.field()

    @strawberry_django.field
    def article_by_slug(self, slug: str) -> ArticleType:
        return Article.objects.get(slug__exact=slug)
    
@strawberry.type
class Mutation:
    @strawberry.mutation
    def create_comment(self, input: CommentInput, article: typing.Optional[ID] = None, comment: typing.Optional[ID] = None) -> CommentType:
        input = dataclasses.asdict(input)
        comment = comment and Comment.objects.get(id=comment)
        article = article and Article.objects.get(id=article)

        comment: Comment = Comment.objects.create(**input, article=article, comment=comment)
        return CommentType(
            article=comment.article,
            creation_date=comment.creation_date,
            id=comment.id,
            text=comment.text,
            author=comment.author,
            responses=comment.responses
        )

    
