import strawberry

import article.schema
import core.schema


@strawberry.type
class Query(
    core.schema.Query,
    article.schema.Query
):
    pass


@strawberry.type
class Mutation(article.schema.Mutation):
    pass


schema = strawberry.Schema(query=Query, mutation=Mutation)
