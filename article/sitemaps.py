from django.contrib.sitemaps import Sitemap

from .models import Article

class ArticleSitemap(Sitemap):
    def items(self):
        return Article.objects.all()
    
    def lastmod(self, obj: Article):
        return obj.last_modification
    
    def location(self, obj: Article) -> str:
        return "/article/"+obj.slug