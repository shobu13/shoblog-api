from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.text import slugify

from shoblog import settings


class User(AbstractUser):
    pass


class Tag(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Content(models.Model):
    name = models.CharField(max_length=500)
    slug = models.SlugField()
    date_creation = models.DateTimeField(auto_now_add=True)
    last_modification = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)

    tags = models.ManyToManyField(to="core.Tag")

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name, allow_unicode=True)
        super().save(*args, *kwargs)

    def __str__(self):
        return self.name
