from article.sitemaps import ArticleSitemap

sitemaps = {
    "articles": ArticleSitemap
}