from django.contrib.syndication.views import Feed
from django.shortcuts import render
from django.urls import reverse

from article.models import Article


class LatestBlogPostFeed(Feed):
    title = "Shoblog articles"
    link = ""
    description = "latest shoblog articles only."

    def items(self):
        return Article.objects.order_by("-date_creation")[:10]

    def item_title(self, item: Article):
        return item.name

    def item_description(self, item: Article):
        return ",".join([i.name for i in item.tags.all()])

    def item_link(self, item):
        return ""
