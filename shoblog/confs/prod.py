SECRET_KEY = "django-insecure-of_t96e=l1-y69890+&@&k+z^__=&&o_)%x-o)*50c=j8)7&aq"

MEDIA_ROOT = BASE_DIR / "media"
STATIC_URL = BASE_DIR / "media"

DEBUG = False

ALLOWED_HOSTS = ["shoblog-api.vraphim.com", "192.168.1.202"]

CORS_ALLOW_ALL_ORIGINS = True
