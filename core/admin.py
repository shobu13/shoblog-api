from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from nested_inline.admin import NestedModelAdmin

from .models import User, Tag


class ContentAdmin(NestedModelAdmin):
    exclude = ("slug", )


admin.site.register(User, UserAdmin)
admin.site.register(Tag)
