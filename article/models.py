from typing import Iterable, Optional
from django.db import models
from django.core.exceptions import ValidationError

from core.models import Content
from core.utils import make_thumbnail

def get_article_image_upload_folder(instance: "ArticleImage", filename):
    """
    return a folder for the saving of the article image in form of article_images/{article.slug}
    """
    return "article_images/"+instance.article.name+"/"+filename


class Article(Content):
    headline = models.TextField(max_length=500)
    thumbnail = models.ImageField(upload_to="article_thumbnail")
    text = models.TextField()

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)


class ArticleImage(models.Model):
    image = models.ImageField(upload_to=get_article_image_upload_folder)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)

class Comment(models.Model):
    """
    A comment must either be link to an article (root comment) or to an another comment (response comment) but not both
    """
    author = models.CharField(max_length=100)
    creation_date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name="comments", null=True, blank=True)
    comment = models.ForeignKey("Comment", on_delete=models.CASCADE, related_name="responses", null=True, blank=True)

    def __str__(self) -> str:
        if self.article:
            return f"{self.article.name} > {self.author} > {self.creation_date}"
        else:
            return f"{self.comment.article} > {self.author} > {self.creation_date}"
    
    def save(self, *args, **kwargs) -> None:

        if not self.article and not self.comment:
            raise ValidationError("either `article` or `comment` attribute must be set.") 
        elif self.article and self.comment:
            raise ValidationError("either `article` or `comment` attribute must be set, not both.")

        return super().save(*args, **kwargs)
